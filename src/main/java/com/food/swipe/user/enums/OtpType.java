package com.food.swipe.user.enums;

public enum OtpType {

	SMS,
	EMAIL
}
