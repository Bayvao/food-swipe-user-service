package com.food.swipe.user.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OtpResponse {

	private String status;
	private String message;
}
